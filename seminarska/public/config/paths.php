<?php
define('URL', 'http://localhost/seminarska/public/');
define('CSS_PATH', 'http://localhost/seminarska/css/');
define('JS_PATH', 'http://localhost/seminarska/js/');
define('ASSETS_PATH', 'http://localhost/seminarska/assets/');
const ROUTES = array(
    'sub',
    'login',
    'error',
    'hello_world',
    'welcome',
    'logout',
    'registration',
    'category',
    'thread',
    'leaderboard'
);
?>