<?php
    require('config/dbConfig.php');
    require('config/paths.php');

    require 'lib/Request.php';
    require 'lib/Router.php';
    require 'lib/Session.php';
    
    #echo URL."\n";
    $request = new Request();
    $router = new Router($request);
    
    $controller_name = $router->route();
    require 'src/controllers/' . $controller_name . '.php';
    $controller = new $controller_name($request);
    $controller->invoke();
    //$controller_name = strtolower($request->get_url_parameters()[2]) . 
     //                   "Controller.php";
    // put src/controllers in config
    //$path = 'src/controllers/' . $controller_name;
    //echo $path;
    //if(file_exists($path)) {
    //    require $path;
    //    $controller_name = rtrim($controller_name, '.php');
    //    $controller = new $controller_name($request);
    //}
    //else {
    //    require 'src/controllers/errorController.php';
    //    $controller = new errorController($request);
    //}
?>