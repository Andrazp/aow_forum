<?php
class Database {
    private $host;
    private $username;
    private $password;
    private $db_name;
    
    function __construct() {
        #echo "Creating a new database";
        $this->host = HOST;
        $this->username = USERNAME;
        $this->password = PASSWORD;
        $this->db_name = DB_NAME;
    }

    public function connect() {
        $connection = new mysqli($this->host, $this->username, 
                                $this->password, $this->db_name);
        if($connection->connect_error) {
            header('Location: '.URL.'error/500');
            return false;
        }
        else{
            #echo "The connection was established\n";
            return $connection;
        }
    }

    //public function get_connection() {
    //    return $this->connection;
    //}

    //public function close() {
    //    if(isset($this->connection)) {
    //        $this->connection->close();
    //        echo "The connection was closed";
    //    }
    //    else {
    //        echo "There was an error closing this connection";
    //    }
    //}
}
?>