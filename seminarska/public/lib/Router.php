<?php
class Router {
    
    private $request;
    private $routes;

    function __construct($request) {
        $this->request = $request;
        $this->routes = ROUTES;
    }

    public function route() {
        $controller_name = strtolower($this->request->get_url_parameters()[0]) . 
        "Controller.php";
        // put src/controllers in config
        $path = 'src/controllers/' . $controller_name;
        // echo $path;
        if(file_exists($path) && 
            in_array($this->request->get_url_parameters()[0], $this->routes)) {
            // require $path;
            $controller_name = rtrim($controller_name, '.php');
            # $controller = new $controller_name($request);
            return $controller_name;
        }       
        else {
            # require 'src/controllers/errorController.php';
            # $controller = new errorController($request);
            //$controller_name = 'errorController';
            //return $controller_name;
            header('Location: '.URL.'error/404');
        }
    }
}
?>