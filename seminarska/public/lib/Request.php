<?php
class Request {
    private $verb;
    private $url_parameters;

    function __construct() {
        $this->verb = $_SERVER['REQUEST_METHOD'];
        $explode_uri = explode('/', $_SERVER['REQUEST_URI']);
        $this->url_parameters = array_slice($explode_uri, 3);
    }

    public function get_verb() {
        return $this->verb;
    }

    public function get_url_parameters() {
        return $this->url_parameters;
    }
}
?>