<?php
    require 'Controller.php';
    class registrationController extends Controller {

        function __construct($request) {
            parent::__construct($request);
            Session::init();
            require('src\models\registrationModel.php');
            require('src/views/registrationView.php');
            $this->model = new registrationModel();
            $this->view = new registrationView();
        }

        public function invoke() {
            if (Session::isset("logged_in") && Session::get("logged_in")) {
                header('Location: '.URL.'welcome');
            }
            else if ($this->request->get_verb() != 'POST') {
                $message = NULL;
                $this->view->render_form($message);
            }
            else if ($this->request->get_url_parameters()[1] == "register") {
                $this->register();
            }
        }

        public function register() {
            if ($this->check_csrf()) {
                if (isset($_POST['username']) && $_POST['username'] != "" &&
                isset($_POST['email']) && $_POST['email'] != "" &&
                isset($_POST['password']) && $_POST['password'] != "" &&
                isset($_POST['confirm']) && $_POST['confirm'] != "") {
                    $username = $_POST['username'];
                    $email = $_POST['email'];
                    $password = $_POST['password'];
                    $confirm_password = $_POST['confirm'];
                    if ($password == $confirm_password) {
                        $message = $this->model->register($username, $email, $password);    
                    }
                    else {
                        $message = "The password must match the confirmation password.";
                    }
                }
                else {
                    $message = "All fields are required.";
                }
            }
            else {
                $message = "The request was not formed on this site.";
            }
            
            $this->view->render_form($message);
            return true;
        }
    }
?>