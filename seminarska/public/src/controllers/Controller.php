<?php
    class Controller {
        protected $request;
        protected $model;
        protected $view;

        function __construct($request) {
            $this->request = $request;
            #echo "This is the main controller.\n";
        }

        public function check_csrf() {
            if (isset($_POST['csrf']) && $_POST['csrf'] != "") {
                $csrf = $_POST['csrf'];
                if ($csrf == Session::get('csrf')) {
                    return true;
                }
            }
        }

    }
?>