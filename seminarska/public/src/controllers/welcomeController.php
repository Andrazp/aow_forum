<?php
    require 'Controller.php';
    class welcomeController extends Controller {

        function __construct($request) {
            parent::__construct($request);
            Session::init();
            require('src/views/welcomeView.php');
            require('src/models/getcontentModel.php');
            $this->view = new welcomeView();
            $this->model = new getcontentModel();
        }

        public function invoke() {
            $results = $this->model->get_categories();
            if ($results) {
                $this->view->invoke($results);
            }
            else {
                echo "There was a problem fetching categories";
            }
            
        }
    }
?>