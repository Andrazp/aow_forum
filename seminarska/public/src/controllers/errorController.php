<?php
    require 'Controller.php';
    class errorController extends Controller {
        
        function __construct($request) {
            parent::__construct($request);
            echo "This is the error controller";
            require('src/views/errorView.php');
            $this->view = new errorView();
        }

        public function invoke() {
            $parameters = $this->request->get_url_parameters();
            #echo gettype($parameters[2]);
            #echo var_dump($parameters);
            #foreach($parameters as $parameter) {
            #    echo $parameter;
            $this->view->invoke($parameters[1]);
        }
    }
?>