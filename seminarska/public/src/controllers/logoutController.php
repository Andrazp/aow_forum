<?php
    require 'Controller.php';
    class logoutController extends Controller{
        
        function __construct($request) {
            parent::__construct($request);
            Session::init();
        }

        public function invoke() {
            Session::destroy();
            header('Location: '.URL.'welcome');
        }
    }
?>