<?php
    require 'Controller.php';
    class categoryController extends Controller{

    function __construct($request) {
        parent::__construct($request);
        Session::init();
        require('src\models\getcontentModel.php');
        require('src\views\categoryView.php');
        $this->model = new getcontentModel();
        $this->view = new categoryView();
    }

    public function invoke() {
        if ($this->request->get_verb() == 'GET') {
            $this->show_threads();
        }
        elseif ($this->request->get_verb() == 'POST') {
            $this->create_thread();
        }
    }

    public function show_threads() {
        if (!empty($this->request->get_url_parameters()[1]) &&
        is_numeric($this->request->get_url_parameters()[1])) {
            $category_id = $this->request->get_url_parameters()[1];
            $num_threads = $this->model->get_number_of_threads($category_id)[0];
            $threads_per_page = 5;
            $total_pages = ceil($num_threads / $threads_per_page);
            
            if (!empty($this->request->get_url_parameters()[2]) &&
            is_numeric($this->request->get_url_paramteres()[2])) {
                $current_page = (int) $this->request->get_url_parameters()[2];
                }
            else {
                $current_page = 1;
            }

            if ($current_page > $total_pages) {
                $current_page = $total_pages;
            }
            elseif ($current_page < 1) {
                $current_page = 1;
            }

            $offset = ($current_page - 1) * $threads_per_page;

            $results = $this->model->get_threads($category_id, $offset, $threads_per_page);
            $this->view->invoke($results, $current_page, $total_pages, $category_id);
            return true;
        }
        else {
            header('Location: '.URL.'error/404');
        }
    }

    public function create_thread() {
        if ((isset($_POST['message'])) && (trim($_POST['message']) != "") &&
            (isset($_POST['title'])) && ($_POST['title'] != "")) {
            $message = $_POST['message'];
            $title = $_POST['title'];
            $category_id = $_POST['category_id'];
            $user_id = Session::get('userid');
            $results = $this->model->create_thread($message, $title, $category_id, $user_id);
            echo $results;
        }
        else {
            echo "ERROR FORM";
        }
    }
}
?>