<?php
    require 'Controller.php';
    class leaderboardController extends Controller{

    function __construct($request) {
        parent::__construct($request);
        Session::init();
        require('src\models\leaderboardModel.php');
        require('src\views\leaderboardView.php');
        $this->model = new leaderboardModel();
        $this->view = new leaderboardView();
    }

    public function invoke() {
        $results = $this->model->get_users_by_wins();
        $this->view->invoke($results);
        return true;
    }
}
?>