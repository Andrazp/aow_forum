<?php
    require 'Controller.php';
    class subController extends Controller {

        function __construct($request) {
            parent::__construct($request);
            echo "This is the sub controller";
            require('src\models\getcontentModel.php');
            require('src/views/subView.php');
            $this->model = new getcontentModel();
            $this->view = new subView();
        }

        public function invoke() {
            $results = $this->model->get_all_threads(1);
            var_dump($results);
            $this->view->invoke();
        }
    }
?>