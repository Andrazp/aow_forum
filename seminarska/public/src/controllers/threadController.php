<?php
    require 'Controller.php';
    class threadController extends Controller{

    function __construct($request) {
        parent::__construct($request);
        Session::init();
        require('src\models\getcontentModel.php');
        require('src\views\threadView.php');
        $this->model = new getcontentModel();
        $this->view = new threadView();
    }

    public function invoke() {
        if ($this->request->get_verb() == 'GET') {
            $this->show_replies();
        }
        elseif ($this->request->get_verb() == 'POST') {
            $this->post_reply();
        }
    }

    public function show_replies() {
        $parameters = $this->request->get_url_parameters();
        if (!empty($parameters[1]) && is_numeric($parameters[1])) {
            $thread_id = $this->request->get_url_parameters()[1];
            $num_posts = $this->model->get_number_of_posts($thread_id)[0];
            $num_posts++; //because thread is already a post and is in the thread table
            $posts_per_page = 5;
            $total_pages = ceil($num_posts / $posts_per_page);

            if (!empty($parameters[2]) && is_numeric($parameters[2])) {
                $current_page = (int) $parameters[2];
                }
            else {
                $current_page = 1;
            }

            if ($current_page > $total_pages) {
                $current_page = $total_pages;
            }
            elseif ($current_page < 1) {
                $current_page = 1;
            }
            
            //because of db design ...
            if ($current_page == 1) {
                $posts_per_page = 4;
            }

            $offset = ($current_page - 1) * $posts_per_page;

            //because of db design ...
            if ($current_page != 1) {
                $offset--; 
            }
            
            $results = $this->model->get_posts($thread_id, $offset, $posts_per_page);
            if ($results == "No rows.") {
                $results = $this->model->get_thread($thread_id);
                $this->view->invoke($results, $current_page, $total_pages, $thread_id);
                return true;
            }
            else {
            $this->view->invoke($results, $current_page, $total_pages, $thread_id);
            return true;
            }
        }
        else {
            header('Location: '.URL.'error/404');
            return false;
        }
    }

    public function post_reply() {
        if ((isset($_POST['message'])) && (trim($_POST['message']) != "")) {
            $message = $_POST['message'];
            $thread_id = $_POST['thread_id'];
            $user_id = Session::get('userid');
            $results = $this->model->post_reply($message, $thread_id, $user_id);
            echo $results;
        }
        else {
            echo "ERROR FORM";
        }
        //$data
        //if ($this->check_csrf()) {
        //    if (isset($_POST['message']) && (trim($_POST['message']) != "") &&
        //        isset($_POST['thread_id']) && ($_POST['thread_id'] != "")) {
        //        $message = $_POST['message'];
        //        $thread_id = $_POST['thread_id'];
        //        $user_id = Session::get('userid');
        //        $results = $this->model->post_reply($message, $thread_id, $user_id);
        //        echo $results;
        //    }
        //    else {
        //        echo "ERROR FORM";
        //    }
       // }
        //else {
        //    echo "CSRF";
        //}
    }
}
?>