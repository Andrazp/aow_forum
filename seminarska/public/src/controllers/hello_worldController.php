<?php
    require 'Controller.php';
    class hello_worldController extends Controller {

        function __construct($request) {
            parent::__construct($request);
            echo "This is the child controller\n";
            require('src\models\hello_worldModel.php');
            require('src/views/hello_worldView.php');
            $this->model = new subModel();
            $this->view = new subView();
        }

        public function invoke() {
            $results = $this->model->create_connection();
            $test = "This is a test";
            $this->view->invoke($test);
        }
    }
?>