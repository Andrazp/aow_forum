<?php
    require 'Controller.php';
    class loginController extends Controller{

    function __construct($request) {
        parent::__construct($request);
        Session::init();
        require('src\models\loginModel.php');
        require('src\views\loginView.php');
        $this->model = new loginModel();
        $this->view = new loginView();
    }

    public function invoke() {
        if (Session::isset("logged_in") && Session::get("logged_in")) {
            header('Location: '.URL.'welcome');
        }
        else if ($this->request->get_verb() != 'POST') {
            $message = 'OK';
            $this->view->render_form($message);
        }
        else if ($this->request->get_url_parameters()[1] == "run") {
            $this->login();
        }
    }

    public function login() {
        if ($this->check_csrf()) {
            if (isset($_POST['username']) && $_POST['username'] != "" &&
                isset($_POST['password']) && $_POST['password'] != "") {
                $username = $_POST['username'];
                $password = $_POST['password'];
                $results = $this->model->check_login($username, $password);
                if ($results) {
                    Session::set('logged_in', true);
                    Session::set('username', $results[0]['username']);
                    Session::set('userid', $results[0]['userid']);
                    header('Location: '.URL.'welcome'); //kamorkoli bomo že šli
                }
                else {
                    $message = "The username or password are incorrect.";
                }
            }
            else {
                $message = "Please enter the username and password.";
            }
        }
        else {
            $message = "The request was not formed on this site.";
        }
        $this->view->render_form($message);
        return true;
    }
}
?>