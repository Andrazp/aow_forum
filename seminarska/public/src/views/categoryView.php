<?php
require('View.php');

class categoryView extends View {

    public function invoke($results, $current_page, $total_pages, $category_id) {
        $pagination_list = parent::generate_pagination_list($current_page, $total_pages);
        parent::start_header();
        parent::include_style('header.css');
        parent::include_style('navbar.css');
        parent::include_style('forum.css');
        parent::include_style('form.css');
        parent::include_script('post_form_ajax.js');
        parent::end_header();
        parent::set_variable('threads', $results);
        parent::set_variable('current_page', $current_page);
        parent::set_variable('pagination_list', $pagination_list);
        parent::set_variable('total_pages', $total_pages);
        parent::set_variable('category_id', $category_id);
        parent::set_template('header');
        parent::set_template('navbar');
        parent::set_template('threads');
        parent::render();
        parent::footer();
    }
}
?>