<?php
require('View.php');

class registrationView extends View {

    public function render_form($message) {
        if ($message == 'OK') {
            parent::start_header();
            parent::end_header();
            parent::set_template('registration_success');
            parent::render();
            parent::footer();
        }
        else {
            $csrf = parent::generate_csrf();
            parent::set_variable('csrf', $csrf);
            Session::set('csrf', $csrf);
            parent::set_variable('message', $message);
            parent::start_header();
            parent::include_style('header.css');
            parent::include_style('navbar.css');
            parent::include_style('forum.css');
            parent::include_style('form.css');
            parent::end_header();
            parent::set_template('header');
            parent::set_template('navbar');
            parent::set_template('registration');
            parent::render();
            parent::footer();
        }
    }
}
?>