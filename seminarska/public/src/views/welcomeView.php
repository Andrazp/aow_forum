<?php
require('View.php');

class welcomeView extends View {

    public function invoke($results) {
        parent::start_header();
        parent::include_style('header.css');
        parent::include_style('navbar.css');
        parent::include_style('forum.css');
        parent::end_header();
        parent::set_variable('categories', $results);
        parent::set_template('header');
        parent::set_template('navbar');
        parent::set_template('welcome');
        parent::render();
        parent::footer();
    }
}
?>