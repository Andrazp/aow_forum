<?php
require('View.php');

class subView extends View {

    function __construct() {
        parent::__construct();
    }

    public function invoke($test) {
        parent::set_variable('test', $test);
        parent::start_header();
        parent::end_header();
        parent::set_template('navbar');
        parent::set_template('hello_world');
        parent::render();
        parent::footer();
    }
}
?>