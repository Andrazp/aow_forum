<?php
require('View.php');

class loginView extends View {

    public function render_form($string) {
        $csrf = parent::generate_csrf();
        parent::set_variable('csrf', $csrf);
        Session::set('csrf', $csrf);
        parent::set_variable('string', $string);
        parent::start_header();
        parent::include_style('header.css');
        parent::include_style('navbar.css');
        parent::include_style('forum.css');
        parent::include_style('form.css');
        parent::end_header();
        parent::set_template('header');
        parent::set_template('navbar');
        parent::set_template('login');
        parent::render();
        parent::footer();
    }
}
?>