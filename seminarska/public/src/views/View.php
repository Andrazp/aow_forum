<?php

class View {

    protected $templates;
    protected $variables;
    
    function __construct() {
        $this->variables = array();
        $this->templates = array();
    }

    public function set_template($template) {
        array_push($this->templates, $template);
    }

    public function set_variable($key, $value) {
        $this->variables[$key] = $value;
    }

    public function render() {
        extract($this->variables);
        foreach ($this->templates as $template) {
            require('templates/' . $template . ".php");
        }
        echo "\n";
    }

    public function include_style($filename) {
        $path = CSS_PATH.$filename;
        echo "<link rel='stylesheet' type='text/css' href='".$path."' />\n";
    }

    public function include_script($filename) {
        $path = JS_PATH.$filename;
        echo "<script type='application/javascript' src='".$path."'></script>\n";
    }

    public function start_header() {
        $string = "<!DOCTYPE html>\n";
        $string .= "<html>\n<head>\n";
        $string .= "<meta charset='UTF-8'>\n";
        $string .= "<title>Age of Wonders: Planetfall</title>\n";
        $string .= "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css'>\n";
        $string .= "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js'></script>\n";
        $string .= "<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' 
        integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>\n";
        $string .= "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script>\n";
        echo $string;
    }

    public function end_header() {
        $string = "</head>\n";
        $string .= "<body>\n";
        echo $string;
    }

    public function footer() {
        //se bo pol dodalo še kej
        echo "<div style=\"background-color: #b3cfdd; height: 20px; bottom: 0; width: 100%;\"><p style=\"text-align: center; vertical-align: middle;
        \">Author: Andraž Pelicon</p></div>";
        #echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js'></script>\n";
        #echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' 
        #integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>\n";
        #echo "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script>\n";
        echo "</body>\n";
        echo "</html>";
    }

    public function generate_csrf() {
        $csrf = bin2hex(random_bytes(32));
        return $csrf;
    }

    public function generate_pagination_list($current_page, $total_pages) {
        $pagination_list = array();
        if ($current_page > 1) {
            array_push($pagination_list, '<<');
        }
        
        $range = 1;
        for ($i = ($current_page - $range); $i < ($current_page + $range + 1); $i++) {
            if (($i > 0) && ($i < $total_pages)) {
                array_push($pagination_list, $i);
            }
        }

        if ($current_page < $total_pages) {
            array_push($pagination_list, '>>');
        }

        return $pagination_list;
    }
}
?>