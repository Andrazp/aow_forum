<?php
require('View.php');

class subView extends View {

    public function invoke() {
        parent::start_header();
        parent::end_header();
        parent::set_template('header');
        parent::set_template('sub');
        parent::render();
        parent::footer();
    }
}
?>