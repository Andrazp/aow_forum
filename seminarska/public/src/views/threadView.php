<?php
require('View.php');

class threadView extends View {

    public function invoke($results, $current_page, $total_pages, $thread_id) {
        $pagination_list = parent::generate_pagination_list($current_page, $total_pages);
        $csrf = parent::generate_csrf();
        Session::set('csrf', $csrf);
        parent::start_header();
        parent::include_style('header.css');
        parent::include_style('navbar.css');
        parent::include_style('forum.css');
        parent::include_style('form.css');
        parent::include_script('post_form_ajax.js');
        parent::end_header();
        parent::set_variable('posts', $results);
        parent::set_variable('current_page', $current_page);
        parent::set_variable('pagination_list', $pagination_list);
        parent::set_variable('total_pages', $total_pages);
        parent::set_variable('thread_id', $thread_id);
        parent::set_variable('csrf', $csrf);
        parent::set_template('header');
        parent::set_template('navbar');
        parent::set_template('posts');
        parent::render();
        parent::footer();
    }
}
?>