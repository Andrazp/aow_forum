<div class="main">
    <div class="form-div">   
    <h1>Registration page</h1>
    <?php # var_dump($_SESSION); ?>
    <?php if (isset($message) && $message != 'OK'): ?>
        <div>
            <p class="alert alert-danger"><?php echo $message; ?></p>
        </div>
        <?php endif; ?>
    <form action="<?php echo URL."registration/register"?>" method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control">
        </div>
        <div class="form-group">
            <label>Confirm password</label>
            <input type="password" name="confirm" class="form-control">
        </div>
        <input type="hidden" name="csrf" value=<?php echo $csrf;?>>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    </div>
</div>