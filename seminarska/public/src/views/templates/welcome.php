    <div class="main">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Categories</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($categories as $category) {
            echo "<tr><td><a href=\"".URL.'category/'.$category['categoryid']."\"><b>" . $category['title'] . 
            "</b></a><br /><p>".$category['description']."</p></td></tr>";
        }
        ?>
        </tbody>
    </table>
    </div>