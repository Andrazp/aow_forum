<div class="main">
    <?php if (gettype($posts) == 'array'): ?>
    <table class="table table-striped .table-bordered">
        <thead>
            <tr>
                <th><?php echo $posts[0]['title']; ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        if ($current_page == 1) {
        echo "<tr><td><p><b>".$posts[0]['thread_owner']."</b></p><p>".
        $posts[0]['thread_date']."</p></td><td><p>
        ".$posts[0]['thread_message']."</p></td></tr>";
        }
        if(array_key_exists('reply_date', $posts[0])) {
            foreach($posts as $post) {
                echo "<tr><td><p><b>".$post['username']."</b> 
                </p><p>".$post['reply_date']."</p></td>
                <td><p>".$post['reply_message']."</p></td></tr>";
            }
        }
        ?>
        </tbody>
    </table>
    <?php else: ?>
    <p class="no-content">There are no threads in this category.</p>
    <?php endif; ?>
    <?php if(!empty($pagination_list)): ?>
    <ul class="pagination pagination-bar">
    <?php
    foreach($pagination_list as $page) {
        if ($page == "<<") {
            echo "<li><a href=\"".URL."thread/".$thread_id."/1\">".$page."</a></li>";
        }
        elseif ($page == ">>") {
            echo "<li><a href=\"".URL."thread/".$thread_id."/".$total_pages."\">".$page."</a></li>";
        }
        else {
            echo "<li><a href=\"".URL."thread/".$thread_id."/".$page."\">".$page."</a></li>";
        }
    }
    ?>
    </ul>
    <?php endif; ?>
</div>
<?php if (Session::isset('logged_in')): ?>
<div class="main">
    <div class="form-div">
        <form id="post-form" method='post' action="<?php echo URL;?>thread/<?php echo $thread_id;?>">
            <div id="post-response"></div>
            <div class="form-group">
                <label>Message:</label>
                <textarea name="message" rows="10" cols="30" class="form-control" id="message">
                </textarea>
            </div>
            <input type="hidden" name="thread_id" value=<?php echo $thread_id;?>>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
<?php endif; ?>