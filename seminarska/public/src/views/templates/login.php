<div class="main">
    <div class = "form-div">
        <h1>Login page</h1>
        <?php if (isset($string) && $string != 'OK'): ?>
        <div>
            <p class="alert alert-danger"><?php echo $string; ?></p>
        </div>
        <?php endif; ?>
        <form action="<?php echo URL; ?>login/run" method="post">
            <div class="form-group">
                <label>Username:</label>
                <input type="text" name="username" class="form-control" id="username">
            </div>
            <div class="form-group">
                <label>Password:</label>
                <input type="password" name="password" class="form-control" id="password">
            </div>
            <input type="hidden" name="csrf" value=<?php echo $csrf;?>>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
</div>
</div>