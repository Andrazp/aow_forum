<div class="main">
    <?php if (gettype($threads) == 'array'): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Threads</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($threads as $thread) {
            echo "<tr><td><a href=\"".URL.'thread/'.$thread['threadid']."\"><b>" . $thread['title'] . 
            "</b></a><br /><p>Author: ".$thread['username']."</p><p style=\"font-style: italic;\">Created: ".$thread['date']."</p></td></tr>";
        }
        ?>
        </tbody>
    </table>
    <?php else: ?>
    <p class="no-content">There are no threads in this category.</p>
    <?php endif; ?>
    <?php if(!empty($pagination_list)): ?>
    <ul class="pagination pagination-bar">
    <?php
    foreach($pagination_list as $page) {
        if ($page == "<<") {
            echo "<li><a href=\"".URL."thread/".$category_id."/1\">".$page."</a></li>";
        }
        elseif ($page == ">>") {
            echo "<li><a href=\"".URL."thread/".$category_id."/".$total_pages."\">".$page."</a></li>";
        }
        else {
            echo "<li><a href=\"".URL."thread/".$category_id."/".$page."\">".$page."</a></li>";
        }
    }
    ?>
    </ul>
    <?php endif; ?>
    </div>
<?php if (Session::isset('logged_in')): ?>
<div class="main">
    <div class="form-div">
        <form id="post-form" method='post' action="<?php echo URL;?>category/<?php echo $category_id;?>">
            <div id="post-response"></div>
            <div>
            <div class="form-group">
                <label>Title:</label>
                <input type="text" name="title" class="form-control" id="title">
            </div>
            <div class="form-group">
                <label>Message:</label>
                <textarea name="message" rows="10" cols="30" class="form-control" id="message">
                </textarea>
            </div>
            <input type="hidden" name="category_id" value=<?php echo $category_id;?>>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
</div>
<?php endif; ?>