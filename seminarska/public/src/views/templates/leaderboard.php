<div class="main">
<?php if (gettype($leaderboard) == 'array'): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>1v1 Leaderboard</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        echo "<tr><td>Place</td><td>Player</td><td>Wins</td></tr>";
        $count = 1;
        foreach($leaderboard as $player) {
            if ($player['wins'] == NULL) {
                echo "<tr><td>".$count."</td><td>".$player['username']."</td><td>--</td></tr>";
            }
            else {
                echo "<tr><td>".$count."</td><td>".$player['username']."</td><td>".
                $player['wins']."</td></tr>";
            }
            $count++;
        }
        ?>
        </tbody>
    </table>
</div>
<?php else: ?>
<p><?php echo $leaderboard; ?></p>
<?php endif;?>

