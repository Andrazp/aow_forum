<?php # var_dump($_SESSION); ?>
<nav class="navbar navbar-default navbar-custom">
  <div class="container-fluid">
  <div class="navbar-header">
    <?php if(Session::isset("username")): ?>
    <p class="navbar-brand">Welcome, <?php echo Session::get("username"); ?></p>
    <?php else: ?>
    <p class="navbar-brand">Welcome, guest!</p>
    <?php endif ?>
  </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo URL;?>welcome">Home</a></li>
        <?php if(Session::isset("logged_in") && Session::get("logged_in")): ?>
        <li><a href="<?php echo URL;?>leaderboard">Leaderboard</a></li>
        <li><a href="<?php echo URL;?>logout">Logout</a></li>
        <?php else: ?>
        <li><a href="<?php echo URL;?>leaderboard">Leaderboard</a></li>
        <li><a href="<?php echo URL;?>login">Login</a></li>
        <li><a href="<?php echo URL;?>registration">Register an account</a></li>
        <?php endif ?>
        </ul>
  </div>
</nav>
<!-- <p>This is a welcome page</p> -->