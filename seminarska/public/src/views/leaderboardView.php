<?php
require('View.php');

class leaderboardView extends View {

    public function invoke($results) {
        parent::start_header();
        parent::include_style('header.css');
        parent::include_style('navbar.css');
        parent::include_style('forum.css');
        parent::end_header();
        parent::set_variable('leaderboard', $results);
        parent::set_template('header');
        parent::set_template('navbar');
        parent::set_template('leaderboard');
        parent::render();
        parent::footer();
    }
}
?>