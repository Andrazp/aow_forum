<?php
require('View.php');

class errorView extends View {

    public function invoke($error) {
        if ($error == 500) {
            parent::start_header();
            parent::include_style('error.css');
            parent::end_header();
            parent::set_template('error500');
            parent::render();
            parent::footer();
        }
        else {
            parent::start_header();
            parent::include_style('error.css');
            parent::end_header();
            parent::set_template('error404');
            parent::render();
            parent::footer();
        }
    }
}
?>