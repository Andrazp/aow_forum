<?php
require('Model.php');
class registrationModel extends Model {

    function __construct() {
        parent::__construct();
    }

    public function register($username, $email, $password) {
        $connection = $this->db->connect();
        $password = password_hash($password, PASSWORD_DEFAULT);
        $stmt = $connection->prepare("INSERT INTO user 
                                    (username, email, password) VALUES (?, ?, ?)");
        $stmt->bind_param("sss", $username, $email, $password);
        if ($stmt->execute()) {
            if ($stmt->affected_rows === 0) {
                return "No rows updated";
            }
            else {
                return "OK";
            }
        }
        else {
            if ($connection->errno === 1062) {
                return "The username or email are already in use.";    
            }
        }
    }
}
?>