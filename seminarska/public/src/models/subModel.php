<?php
require('Model.php');
class subModel extends Model {

    function __construct() {
        parent::__construct();
    }

    public function getUsers () {
        $connection = $this->db->connect();
        $results = $connection->query("SELECT * FROM user;");
        $results = $results->fetch_all(MYSQLI_ASSOC);
        $connection->close();
        return $results;
    }
}
?>