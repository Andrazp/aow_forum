<?php
require("Model.php");

class loginModel extends Model {
    
    function __construct() {
        parent::__construct();
    }

    public function check_login($username, $password) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT username, password, userid FROM user WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if (!$arr) {
            $stmt->close();
            $connection->close();
            return false;
        }
        else {
            if (password_verify($password, $arr[0]['password'])) {
                $stmt->close();
                $connection->close();
                return $arr;
            }
            else {
                $stmt->close();
                $connection->close();
                return false;
            }
        }
    }
}
?>