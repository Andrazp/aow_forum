<?php
require('Model.php');
class getcontentModel extends Model {

    function __construct() {
        parent::__construct();
    }

    public function get_categories() {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT categoryid, title, description FROM category;");
        $stmt->execute();
        $results = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if (!$results) {
            $stmt->close();
            $connection->close();
            return false;
        }
        else {
            $stmt->close();
            $connection->close();
            return $results;
        }
    }

    public function get_threads($category, $offset, $threads_per_page) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT t.title, t.threadid, DATE_FORMAT(t.timestamp, '%d/%m/%Y %H:%i') AS date, 
                u.username FROM thread AS t, category AS c, user AS u WHERE t.categoryid = c.categoryid AND
                t.categoryid = ? AND t.userid = u.userid ORDER BY t.threadid ASC LIMIT ?,?;");
        $stmt->bind_param("iii", $category, $offset, $threads_per_page);
        $stmt->execute();
        $results = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(!$results) {
            $stmt->close();
            $connection->close();
            $results = "No rows.";
        }
        else {
            $stmt->close();
            $connection->close();
            #var_dump($results);
            return $results;
        }
    }

    public function get_thread($thread_id) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT t.title, u.username AS thread_owner, 
                                    t.message AS thread_message,
                                    DATE_FORMAT(t.timestamp, '%d/%m/%Y %H:%i') AS thread_date
                                    FROM thread AS t, user AS u
                                    WHERE t.threadid = ? AND
                                    t.userid = u.userid;");
        $stmt->bind_param("i", $thread_id);
        $stmt->execute();
        $results = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if (!$results) {
            $stmt->close();
            $connection->close();
            $results = "No rows.";
        }
        else {
            $stmt->close();
            $connection->close();
        }
        return $results;
    }

    public function get_posts($thread_id, $offset, $posts_per_page) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT r.replyid, r.message AS reply_message, 
                            DATE_FORMAT(r.timestamp, '%d/%m/%Y %H:%i')
                            AS reply_date, u.username, t.message AS thread_message, 
                            t.title, (SELECT u.username FROM user AS u, 
                            thread AS t WHERE u.userid = t.userid AND 
                            t.threadid = ?) AS thread_owner,
                            DATE_FORMAT(t.timestamp, '%d/%m/%Y %H:%i') AS thread_date
                            FROM reply AS r, user AS u, thread AS t
                            WHERE t.threadid = ? AND 
                            r.threadid = t.threadid AND 
                            u.userid = r.userid
                            ORDER BY replyid ASC LIMIT ?,?;");
        $stmt->bind_param("iiii", $thread_id, $thread_id, $offset, $posts_per_page);
        $stmt->execute();
        $results = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if (!$results) {
            $stmt->close();
            $connection->close();
            $results = "No rows.";
        }
        else {
            $stmt->close();
            $connection->close();
        }
        return $results;
    }

    public function get_number_of_threads($category_id) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT COUNT(*) FROM thread WHERE categoryid = ?");
        $stmt->bind_param("i", $category_id);
        $stmt->execute();
        $results = $stmt->get_result()->fetch_row();
        $stmt->close();
        $connection->close();
        return $results;
    }

    public function get_number_of_posts($thread_id) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT COUNT(*) FROM reply WHERE threadid = ?");
        $stmt->bind_param("i", $thread_id);
        $stmt->execute();
        $results = $stmt->get_result()->fetch_row();
        $stmt->close();
        $connection->close();
        return $results;
    }

    public function post_reply($message, $thread_id, $user_id) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("INSERT INTO reply (message, threadid, userid) VALUES (?,?,?)");
        $stmt->bind_param("sii", $message, $thread_id, $user_id);
        $stmt->execute();
        if ($stmt->affected_rows === 0) {
            $stmt->close();
            $connection->close();
            return "SYSTEM ERROR";
        }
        else {
            $stmt->close();
            $connection->close();
            return "OK";
        }
    }

    public function create_thread($message, $title, $category_id, $user_id) {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("INSERT INTO thread (message, title, categoryid, userid) VALUES (?,?,?,?)");
        $stmt->bind_param("ssii", $message, $title, $category_id, $user_id);
        $stmt->execute();
        if ($stmt->affected_rows === 0) {
            $stmt->close();
            $connection->close();
            return "SYSTEM ERROR";
        }
        else {
            $stmt->close();
            $connection->close();
            return "OK";
        }

    }
}
?>