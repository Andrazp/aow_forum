<?php
require('Model.php');
class leaderboardModel extends Model {

    function __construct() {
        parent::__construct();
    }

    public function get_users_by_wins() {
        $connection = $this->db->connect();
        $stmt = $connection->prepare("SELECT username, wins
                                    FROM user
                                    ORDER BY wins DESC;");
        $stmt->execute();
        $results = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if (!$results) {
            $stmt->close();
            $connection->close();
            $results = "No games have been played yet.";
            return $results;
        }
        else {
            $stmt->close();
            $connection->close();
            return $results;
        }
    }
}
?>