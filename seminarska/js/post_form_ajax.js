$(document).ready(function() {
  $('#post-form').submit(function(event) {
    console.log("Smo v Ajaxu.")
    event.preventDefault(); // Prevent the form from submitting via the browser
    var form = $(this);
    var serialized_form = form.serialize();
    console.log(serialized_form);
    console.log(typeof serialized_form);
    $.ajax({
      type: 'POST',
      url: form.attr('action'),
      data: form.serialize()
    }).done(function(data) {
      var post_response = document.getElementById('post-response');
      //post_response.innerHTML = data;
      //post_response.classList.add("alert");
      //post_response.classList.add("alert-success");
      if (data == 'OK') {
        post_response.innerHTML = "Your message was succesfully posted.";
        post_response.classList.add("alert");
        post_response.classList.add("alert-success");
      }
      else if (data == 'ERROR FORM'){
        post_response.innerHTML = "All fields are required.";
        post_response.classList.add("alert");
        post_response.classList.add("alert-danger");
      }
      else {
        post_response.innerHTML = "Your message was not posted due to a system error. Please try again later.";
        post_response.classList.add("alert");
        post_response.classList.add("alert-danger");
      }
    }).fail(function(data) {
      post_repsonse = document.getElementById('post_response');
      post_response.innerHTML = "Your message was not posted due to a system error. Please try again later.";
      post_response.classList.add("alert");
      post_response.classList.add("alert-danger");
    });
  });
});