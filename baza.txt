CREATE DATABASE aow_final_test;
USE aow_final_test;

CREATE TABLE user (userid INT NOT NULL AUTO_INCREMENT, 
username VARCHAR(40) NOT NULL, 
email VARCHAR(40) NOT NULL, 
password VARCHAR(255) NOT NULL,
wins INT,
active INT NOT NULL DEFAULT 1,
PRIMARY KEY (userid),
FOREIGN KEY (active) REFERENCES activity_domain(activityid),
UNIQUE (username),
UNIQUE (email));

CREATE TABLE activity_domain (activityid INT NOT NULL AUTO_INCREMENT,
type VARCHAR(10),
PRIMARY KEY (activityid));

CREATE TABLE thread (threadid INT NOT NULL AUTO_INCREMENT,
message TEXT,
timestamp DATETIME DEFAULT NOW(),
userid INT,
categoryid INT,
PRIMARY KEY (threadid),
FOREIGN KEY (userid) REFERENCES user (userid),
FOREIGN KEY (categoryid) REFERENCES category (categoryid));

CREATE TABLE reply (replyid INT NOT NULL AUTO_INCREMENT,
message TEXT,
timestamp DATETIME DEFAULT NOW(),
threadid INT,
PRIMARY KEY (replyid),
FOREIGN KEY (threadid) REFERENCES thread (threadid));

CREATE TABLE category (categoryid INT NOT NULL AUTO_INCREMENT,
title VARCHAR(255),
timestamp DATETIME DEFAULT NOW(),
PRIMARY KEY (categoryid));

INSERT INTO activity_domain (type) VALUES ('yes');

INSERT INTO activity_domain (type) VALUES ('no');

INSERT INTO user (username, email, password, wins) VALUES ('Flash', 'flash@email.com', '$2y$10$QrlrCwwi8mTvqvoi22Y3j.y4JMhUQX1BxOqxWM.sYbHpjZElo8spq', 8);
INSERT INTO user (username, email, password, wins) VALUES ('Jaedong', 'jaedong@email.com', '$2y$10$RUv93BXhnVlD/9JtZY6eKuVUz10k3Vi5vVsqTFCytXOmIC6OFWqwq', 7);
INSERT INTO user (username, email, password, wins) VALUES ('Rain', 'rain@email.com', '$2y$10$EvqL8lyQv9X4RCFWu3gux.IZGTOf2UFpyN4XWotkLS/bksg.1XcmS', 5);